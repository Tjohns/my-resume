function splashscreenPage() {
    setTimeout(showPage, 2000);
    onClickHome();
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("content").style.display = "block";
}

function onClickHome() {
  $(document).ready(function(){
    $.get("/html5/home.html", function(data) {
      $("#content").html(data);
    });
  });

closeNav();
}

function onClickExperience() {
  $(document).ready(function (){
    $.get("/html5/experience.html", function(data) {
      $("#content").html(data);
    });
  });

closeNav();
}

function onClickEducation() {
$(document).ready(function (){
  $.get("/html5/education.html", function(data) {
    $("#content").html(data);
  });
});

closeNav();
}

function onClickSkills() {
$(document).ready(function (){
  $.get("/html5/skills.html", function(data) {
    $("#content").html(data);
  });
});

closeNav();
}

function onClickProjects() {
$(document).ready(function (){
  $.get("/html5/projects.html", function(data) {
    $("#content").html(data);
  });
});

closeNav();
}

function onClickContacts() {
$(document).ready(function (){
  $.get("/html5/contacts.html", function(data) {
    $("#content").html(data);
  });
});

closeNav();
}

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("container").style.marginLeft = "250px";
  document.body.style.backgroundColor = "rgba(0,0,0,0.9)";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("container").style.marginLeft= "0";
  document.body.style.backgroundColor = "white";
}
